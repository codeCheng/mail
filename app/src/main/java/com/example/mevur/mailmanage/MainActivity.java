package com.example.mevur.mailmanage;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sun.mail.util.logging.MailHandler;

import javax.mail.internet.NewsAddress;

public class MainActivity extends AppCompatActivity {

    private int QQMAIL = 1;
    private int NETEASY = 2;
    private int type;
    private Dialog login_dialog;
    private ImageView qq;
    private ImageView neteasy;
    private String username;
    private String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //透明导航栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        ((EditText) findViewById(R.id.edt_account)).setText("@163.com");
        Button ok = (Button) findViewById(R.id.btn_login);
        ok.setOnClickListener(new ClickListener());
        Button cancel = (Button) findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new ClickListener());
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_login: {
                    View parent = view.getRootView();
                    username = ((EditText) parent.findViewById(R.id.edt_account)).getText().toString();
                    password = ((EditText) parent.findViewById(R.id.edt_password)).getText().toString();
                    if (username.trim().equals("") || password.trim().equals("")) {
                        Toast.makeText(MainActivity.this, "账户和密码不能为空", Toast.LENGTH_LONG).show();
                    }
                    else {
                        new LoginTask().execute(username,password);
                    }
                    break;
                }
                case R.id.btn_cancel:
                    finish();
                    break;

                default:
                    Toast.makeText(MainActivity.this, "无效操作", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    private void login(int type) {
        //LayoutInflater inflater = getLayoutInflater();
        //View view = inflater.inflate(R.layout.login_dialog, (ViewGroup) findViewById(R.id.login_dialog));
        //login_dialog = new Dialog(this);
        //login_dialog.setContentView(view);
       // login_dialog.show();
    }

    private class LoginTask extends AsyncTask<String,Void,Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(MainActivity.this, "登录成功", Toast.LENGTH_LONG).show();
                //login_dialog.cancel();
                Intent intent = new Intent(MainActivity.this,MailView.class);
                intent.putExtra("user",username);
                intent.putExtra("paw",password);
                startActivity(intent);
            }
            else {
                Toast.makeText(MainActivity.this, "登录失败,检查用户名和密码", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            return MailHelper.login(strings[0], strings[1]);
        }
    }
}
