package com.example.mevur.mailmanage;

import android.content.Context;
import android.content.SharedPreferences;
import android.renderscript.ScriptGroup;
import android.util.Base64;
import android.util.Log;

import com.sun.mail.util.MimeUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

/**
 * Created by Mevur on 2016/11/17.
 */

public class MailHelper {

    public static boolean login(String username, String password) {
        String host = "smtp.163.com";
        Socket client = null;
        try {
            client = new Socket(host, 25);
            InputStream is = client.getInputStream();
            BufferedReader resoonse = new BufferedReader(new InputStreamReader(is));
            OutputStream os = client.getOutputStream();
            PrintWriter request = new PrintWriter(os, true);
            Log.i("res", resoonse.readLine());
            //发送helo
            request.println("HELO " + username);
            Log.i("res", resoonse.readLine());
            //对用户名和密码进行base64编码
            username = Base64.encodeToString(username.getBytes(), Base64.NO_WRAP);
            password = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
            //请求登录
            request.println("auth login");
            Log.i("res", resoonse.readLine());
            //输入用户名
            request.println(username);

            Log.i("res", resoonse.readLine());
            //输入密码
            request.println(password);
            //返回验证结果
            String res = resoonse.readLine();

            if (res.startsWith("235")) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e) {
            return false;
        }
        finally {
            try {
                if (client != null) {
                    client.close();
                }
            }
            catch (IOException e) {}
        }
    }

    public static boolean sendMail(String to, String subject, String content, String username, String password) {
        String host = "smtp.163.com";
        Socket client = null;
        try {
            client = new Socket(host, 25);
            InputStream is = client.getInputStream();
            BufferedReader resoonse = new BufferedReader(new InputStreamReader(is));
            OutputStream os = client.getOutputStream();
            PrintWriter request = new PrintWriter(os, true);
            Log.i("res", resoonse.readLine());
            //发送helo
            request.println("HELO " + username);
            Log.i("res", resoonse.readLine());
            //对用户名和密码进行base64编码
            String ausername = Base64.encodeToString(username.getBytes(), Base64.NO_WRAP);
            String apassword = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);

            //认证
            request.println("auth login");
            Log.i("res",resoonse.readLine());

            request.println(ausername);
            Log.i("res",resoonse.readLine());

            request.println(apassword);
            Log.i("res",resoonse.readLine());
            //认证结束

            request.println("mail from: <" + username + ">");
            Log.i("res",resoonse.readLine());

            request.println("rcpt to: <" + to + ">");
            Log.i("res",resoonse.readLine());

            request.println("data");
            Log.i("res",resoonse.readLine());

            request.println("Subject:" + subject);
            request.println("from:" + username);
            request.println("to" + to);
            request.println("Content-Type: text/plain;charset=\"gb2312\"");
            request.println();
            request.println(content);
            request.println();
            request.println("\r\n.\r\n");
            String res = resoonse.readLine();
            Log.i("res",res);
            request.println("rset");
            Log.i("res", resoonse.readLine());

            request.println("quit");
            Log.i("res",resoonse.readLine());


            if (client != null) {
                client.close();
            }
            if (res.startsWith("250")) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean sendMailWithJAVAMAIL(String to, String subject, String content, final String username, final String password) {
        String host = "smtp.163.com";
        try {
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", host);
            properties.setProperty("mail.smtp.auth", "true");
            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username,password);
                }
            });
            MimeMessage msg = new MimeMessage(session);
            InternetAddress fromAddress = new InternetAddress(username);
            msg.setFrom(fromAddress);
            InternetAddress toAddress = new InternetAddress(to);
            msg.addRecipient(Message.RecipientType.TO, toAddress);
            msg.setSubject(subject);
            msg.setText(content);
            msg.setSentDate(new Date());
            Transport transport = session.getTransport("smtp");
            transport.connect(host,username,password);
            transport.sendMessage(msg, new Address[] {toAddress});
            transport.close();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static List<Mail> readMail(String username, String password) {
        List<Mail> mails = new ArrayList<Mail>();
        try {
            String host = "pop.163.com";
            Properties props = new Properties();
            Session session = Session.getInstance(props, null);
            Store store = session.getStore("pop3");
            store.connect(host, username, password);
            Folder folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);
            Message[] msgs = folder.getMessages();
            for (int i = 0; i < msgs.length; i++) {
                Mail mail = new Mail();
                mail.setFrom(MimeUtility.decodeText(msgs[i].getFrom()[0].toString()));
                Date date = msgs[i].getSentDate();
                mail.setDate(date.toString());
                mail.setSubject(MimeUtility.decodeText(msgs[i].getSentDate().toString()));
                ByteArrayOutputStream bao = new ByteArrayOutputStream(1024);
                PrintStream cache = new PrintStream(bao);
                PrintStream old = System.out;
                System.setOut(cache);
                msgs[i].writeTo(System.out);
                mail.setContent(MimeUtility.decodeText(bao.toString()));
                mails.add(mail);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return mails;
        }
    }




}


