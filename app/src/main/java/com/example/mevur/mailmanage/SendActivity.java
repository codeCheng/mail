package com.example.mevur.mailmanage;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SendActivity extends AppCompatActivity {

    String username;
    String password;
    String to;
    String sub;
    String content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        password = intent.getStringExtra("password");
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                to = ((EditText) findViewById(R.id.sent_to)).getText().toString();
                sub = ((EditText) findViewById(R.id.send_subject)).getText().toString();
                content = ((EditText) findViewById(R.id.send_content)).getText().toString();
                if (to.equals("") || sub.equals("") || content.equals("")) {
                    Toast.makeText(SendActivity.this, "请检查所有数据是否填写", Toast.LENGTH_LONG).show();
                    return;
                }
                new Sender().execute();
            }
        });
    }

    private class Sender extends AsyncTask<Void,Void,Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return MailHelper.sendMail(to,sub,content,username,password);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(SendActivity.this,"发送成功",Toast.LENGTH_LONG).show();
                try {

                    Thread.sleep(Toast.LENGTH_LONG);
                    finish();
                }
                catch (Exception e) {

                }
            }
            else {
                Toast.makeText(SendActivity.this,"发送失败",Toast.LENGTH_LONG).show();
            }
        }
    }
}
