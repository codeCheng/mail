package com.example.mevur.mailmanage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Mevur on 2016/11/17.
 */

public class MailDB {
    private String NAME = "mail_db";
    private int VERSION = 1;
    private SQLiteDatabase db;
    private MailDB mailDB;

    private MailDB(Context context) {
        DBHelper dbHelper = new DBHelper(context, NAME, null, VERSION);
        db = dbHelper.getWritableDatabase();
    }

    public synchronized MailDB getInstance(Context context) {
        if (mailDB == null) {
            mailDB = new MailDB(context);
        }
        return mailDB;
    }

    public void storageMail(Mail mail) {
        ContentValues values = new ContentValues();
        values.put("ischeck","false");
        values.put("date",mail.getDate());
        values.put("from",mail.getFrom());
        values.put("to",mail.getTo());
        values.put("subject",mail.getSubject());
        values.put("content",mail.getContent());
        db.insert("mail", null, values);
    }
    public boolean updateMail(String id) {
        String selection = "id=?";
        String[] args = {id};
        ContentValues values = new ContentValues();
        values.put("ischeck","false");
        int i = db.update("mail", values, selection, args);
        if (i > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
