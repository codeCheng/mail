package com.example.mevur.mailmanage;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailView extends AppCompatActivity {
    private String username;
    private String password;
    ListView malist;
    List<Mail> mailes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_view);
        malist = (ListView) findViewById(R.id.mail_list);
        Intent intent = getIntent();
        username = intent.getStringExtra("user");
        password = intent.getStringExtra("paw");
        new ReadMail().execute(username,password);
    }
    private class ReadMail extends AsyncTask<String,Void,List<Mail>> {
        @Override
        protected List<Mail> doInBackground(String... strings) {

            return MailHelper.readMail(username,password);
        }

        @Override
        protected void onPostExecute(final List<Mail> mails) {
            if (mails.size() > 0) {
                List<Map<String, Object>> data = new ArrayList<>();
                for (Mail mail : mails) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("subject",mail.getSubject());
                    map.put("date", mail.getDate());
                    String from = mail.getFrom();
                    if (from.length() > 20) {
                        from = from.substring(0,20);
                    }
                    map.put("from"," *" + from);
                    String str = mail.getContent();
                    if (str.length() > 30) {
                        str = str.substring(0,30) + "...";
                    }
                    map.put("content",str);
                    data.add(map);
                    ((Button) findViewById(R.id.send)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MailView.this, SendActivity.class);
                            intent.putExtra("username",username);
                            intent.putExtra("password",password);
                            startActivity(intent);
                        }
                    });
                }

                SimpleAdapter adapter = new SimpleAdapter(MailView.this,data, R.layout.mail_item,
                                        new String[] {"from","subject","date","content"},
                                        new int[]{R.id.from,R.id.subject,R.id.time,R.id.preview});
                malist.setAdapter(adapter);
                malist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(MailView.this, MailInfo.class);
                        intent.putExtra("content",mails.get(i).getContent());
                        startActivity(intent);
                    }
                });
            }
        }
    }
}
