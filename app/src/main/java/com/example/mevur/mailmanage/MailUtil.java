package com.example.mevur.mailmanage;

import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

/**
 * Created by Mevur on 2016/11/18.
 */

public class MailUtil {

    public final static int RECEIVE_DATE = 1;
    public final static int SENT_DATE = 2;
    public static String getSubject(MimeMessage msg) throws UnsupportedEncodingException,MessagingException {
        return MimeUtility.decodeText(msg.getSubject());
    }

    public static String getFrom(MimeMessage msg) throws UnsupportedEncodingException, MessagingException {
        String from = "";
        Address[] froms = msg.getFrom();
        if (froms.length < 1) {
            throw new MessagingException("没有发件人");
        }
        InternetAddress address = (InternetAddress) froms[0];
        String personal = address.getPersonal();
        if (personal != null) {
            personal = MimeUtility.decodeText(personal) + " ";
        }
        else {
            personal = "";
        }
        from = personal + "<" + address.getAddress() + ">";
        return from;
    }

    public static String getReceiveAddress(MimeMessage msg, MimeMessage.RecipientType type)
        throws MessagingException {
        StringBuffer receive = new StringBuffer();
        Address[] address = null;
        if (type == null) {
            address = msg.getAllRecipients();
        }
        else {
            address = msg.getRecipients(type);
        }
        if (address == null || address.length < 1) {
            throw new MessagingException("没有收件人");
        }
        for (Address adr : address) {
            InternetAddress internetAddress = (InternetAddress) adr;
            receive.append(internetAddress.toUnicodeString()).append("|");
        }
        receive.deleteCharAt(receive.length() - 1);
        return receive.toString();
    }

    public static String getDate(MimeMessage msg,int type) throws MessagingException {
        Date date;
        if (type == SENT_DATE) {
            date = msg.getSentDate();
        }
        else {
            date = msg.getReceivedDate();
        }
        String format = "yyyy年MM月dd日 E HH:mm";
        return new SimpleDateFormat(format).format(date);
    }

    public static String getContent(Part part)
           throws MessagingException, IOException {
        StringBuffer bodytext = new StringBuffer();
        String type = part.getContentType();
        boolean conname = false;
        int nameindex = type.indexOf("name");
        if (nameindex != -1) {
            conname = true;
        }
        Log.i("type",type);
        if (part.isMimeType("text/plain") && !conname) {
            bodytext.append((String) part.getContent());
        }
        else if (part.isMimeType("text/html") && !conname) {
            MailUtil.getContent((Part) part.getContent());
        }
        else if(part.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) part.getContent();
            int counts = multipart.getCount();
            for (int i = 0; i < counts;  i++) {
                MailUtil.getContent(multipart.getBodyPart(i));
            }
        }
        else if (part.isMimeType("message/rfc822")) {
            MailUtil.getContent((Part) part.getContent());
        }
        return bodytext.toString();
    }
}
